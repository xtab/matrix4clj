(defproject matrix4clj "0.0.1-SNAPSHOT"
  :description "A Matrix protocol client library for Clojure aimed at developing bots."
  :url "http://example.com/FIXME"
  :license {:name "BSD-3-Clause"
            :url "https://opensource.org/licenses/BSD-3-Clause"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/core.async "1.1.587"]
                 [clj-http "3.10.0"]
                 [cheshire "5.10.0"]
                 [miikka/long-thread "0.3.0"]]
  :repl-options {:init-ns matrix4clj.core})
