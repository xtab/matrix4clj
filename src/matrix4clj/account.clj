(ns matrix4clj.account
  (:require [matrix4clj.api :as api]
            [matrix4clj.util :refer :all]))

(defn login
  "Logs into a homeserver. If successful, returns the context object with the
  server-supplied access-key added to it."
  {:arglists '([context])}
  [{:keys [config] :as context}]
  (let [{:keys [user_id access_token device_id well_known]}
        (api/post context "login" {:form-params (config->login config)})]
    (-> context
        (merge {:access-token access_token
                :user-id user_id})
        (assoc-in [:server :base-url]
                  (if-let [url (get-in well_known [:m.homeserver :base_url])]
                    url
                    (get-in context [:server :base-url])))
        (assoc-in [:config :device :id] device_id))))


(defn logout
  "Logs out from a homeserver. If successful, returns the context object with the
  server-supplied access-key removed."
  [context]
  (api/post context "logout")
  (invalidate-session context))

(defn logout-all
  "Invalidates all access tokens associated with the user."
  [context]
  (api/post context "logout" "all")
  (invalidate-session context))
