(ns matrix4clj.sync
  "Contains the core functions used to handle incoming events."
  (:require [matrix4clj.api :as api]
            [clojure.core.async :as async :refer [>! >!! <! <!! close! put! go go-loop chan]]
            [long-thread.core :as long-thread]
            [clojure.set :as set]
            [clojure.string :as str]
            [cheshire.core :as json])
  (:use [matrix4clj.util])
  (:refer-clojure :exclude [sync]))

(defn sync
  "Sends a sync request to the server.

  This function takes extra arguments as key-value pairs: :since, :filter
  and :timeout."
  [context & {:keys [since filter timeout] :as args}]
  (let [filter-param (if (map? filter) (json/generate-string filter) filter)
        query-params (->> {:filter filter-param
                           :since since
                           :timeout timeout}
                          (remove (comp nil? second))
                          (into {}))
        result (promise)]
    (api/async-get context "sync" {:query-params query-params
                                   :respond #(deliver result (:body %))
                                   :raise #(deliver result (:error %))})
    @result))

(defn user-joins-event?
  "Returns true if the supplied timeline event is an event involving the user with
  the given ID joining a room."
  {:arglists '([user-id event])}
  [user-id
   {{e-membership :membership} :content
    e-type :type
    e-state-key :state_key}]
  (and (= e-type "m.room.member")
       (= e-membership "join")
       (= e-state-key user-id)))

(defn remove-newly-joined-room-updates
  "Called by room-status-update-filter to filter out historical status updates
  from newly-joined rooms."
  [sync-result my-uid new-rooms]
  (letfn [(not-my-join-event? [e]
            (not (user-joins-event? my-uid e)))
          (redact-timeline [tl]
            (assert (not (nil? tl)))
            (let [[newer older]
                  (->> tl vec rseq (split-with not-my-join-event?))]
              (if (seq older)
                (into (vector (first older)) (reverse newer))
                [])))]
    (reduce (fn [cur-result room]
              (update-in cur-result
                         [:rooms :join room :timeline :events]
                         redact-timeline))
            sync-result new-rooms)))

(defn linearize-room-events
  "Transforms the rooms key from an event update received from the server into a
  vector of event objects. The argument is the contents of the :rooms subkey.

  The room ID is added to the event as a :room_id key (as the spec normally
  requires for events), the new membership state (:join, :invite or :leave) is
  added as a :room_membership key and the event
  kind (i.e. :timeline, :state, :ephemeral, :invite_state is added as
  an :event_kind key."
  [room-event-update]
  (let [valid-subkeys {:join [:timeline :state :ephemeral]
                       :leave [:timeline :state]
                       :invite [:invite_state]}]
    (apply concat
           (for [k [:join :leave :invite]
                 room (keys (k room-event-update))
                 sk (k valid-subkeys)]
             (->> (get-in room-event-update [k room sk :events])
                  (map #(merge % {:room_id (name room)
                                  :room_membership k
                                  :event_kind sk})))))))

(defn linearize-events
  "Transforms an event update received from the server into a vector of event
  objects, making it easier to consume these events sequentially.

  TODO: Linearize other events as well, not just room events. If so, just add
  them as arguments for the concat form."
  [event-update]
  (concat (linearize-room-events (:rooms event-update))))

(defn first-sync
  "Syncs the state for the first time: calls the sync API endpoint, but limiting
  timeline events to only the last one."
  [context]
  (sync context :filter {:room {:timeline {:limit 1}}}))

(defn sync-loop
  "Given a context and a channel, calls sync repeatedly to stream the event
  updates to that channel.

  This function is designed to be run inside a long-thread. If interrupted at
  any point, the channel is automatically closed."
  [context channel]
  (try
    (loop [{next-batch :next_batch :as response} (first-sync context)]
      (when-not (:error response)
        (when (>!! channel response)
          (recur (sync context :since next-batch :timeout 30000)))))
    (catch java.lang.InterruptedException _
      (close! channel)
      nil)))


(defn room-status-update-filter
  "Transducer filtering out all timeline events involving rooms the client has
  just joined."
  [my-user-id]
  (fn [xf]
    (let [in-rooms (volatile! #{})]
      (fn
        ([] (xf))
        ([result] (xf result))
        ([result input]
         (let [jr (-> (get-in input [:rooms :join]) keys set)
               lr (-> (get-in input [:rooms :leave]) keys set)
               new-rooms (set/difference jr @in-rooms)]
           (vreset! in-rooms (set/difference (set/union @in-rooms jr) lr))
           (xf result
               (remove-newly-joined-room-updates input
                                                 my-user-id
                                                 new-rooms))))))))


