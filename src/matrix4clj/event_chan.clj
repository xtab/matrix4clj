(ns matrix4clj.event-chan
  "Contains functions controlling the event channel associated with a context
  object."
  (:require [matrix4clj.util :refer [whoami]]
            [matrix4clj.sync :refer [sync-loop
                                     room-status-update-filter
                                     linearize-events]]
            [clojure.core.async :as async :refer [chan close! go-loop <!]]
            [long-thread.core :as long-thread]))

(def ^:dynamic *event-chan-buffer-size* 1)

(defn event-chan
  "Creates an event channel."
  [my-user-id]
  (chan *event-chan-buffer-size*
        (comp (room-status-update-filter my-user-id)
              (mapcat linearize-events))))

(defn event-processor
  "Starts a loop, processing each event received on the input channel using a
  custom handler function. Stops if the channel is closed."
  [context channel handlerfn]
  (go-loop []
    (when-let [event (<! channel)]
      (try
        (handlerfn context event)
        (catch Exception e
          (binding [*out* *err*]
            (println "Exception while handling event.")
            (.printStackTrace e))))
      (recur))))

(defn start-event-loop
  [context handlerfn]
  (let [ch (event-chan (whoami context))]
    (event-processor context ch handlerfn)

    {:channel ch
     :sync-loop-thread (long-thread/create "sync-loop"
                                           #(sync-loop context ch))}))

(defn stop-event-loop
  [{:keys [sync-loop-thread channel]}]
  (long-thread/stop sync-loop-thread)
  (close! channel))


