(ns matrix4clj.api
  (:require [clj-http.client :as client]
            [clojure.string :as str]
            [cheshire.core :as json])
  (:import [java.net URLEncoder])
  (:refer-clojure :exclude [get update]))

(def ^:dynamic *default-http-params*
  "Don't use cookies; accept only JSON and coerce all responses to JSON."
  {:cookie-policy :none
   :accept :json
   :as :json})

(defmacro defhttpmethod
  "Defines a wrapper around the request function for a given HTTP method
  symbol (e.g. :get)."
  [method]
  `(do
     (defn ~(symbol method)
       {:arglists '([~'context & ~'args])}
       [context# & args#]
       (apply request ~method context# args#))
     (defn ~(symbol (str "async-" (name method)))
       {:arglists '([~'context & ~'args])}
       [context# & args#]
       (apply async-request ~method context# args#))))

(defmacro defhttpmethods
  "Calls defhttpmethod for every parameter."
  [& methods]
  (let [forms (map #(list 'defhttpmethod %) methods)]
    `(do ~@forms)))

(defn url-encode
  [s]
  (URLEncoder/encode s "UTF-8"))

(defn url-for
  "Concatenates a base URL, the string \"/_matrix/client/r0/\" and a list of
  path components with slashes in between, to yield the URL of a Matrix API
  endpoint."
  [base components]
  (str base
       (if (not (str/ends-with? base "/")) "/")
       "_matrix/client/r0/"
       (str/join "/" (map url-encode components))))

(defn request-params
  "Merges the method, the request URL, an access key and a map with extra
  parameters with the default HTTP parameters in order to create a request
  parameters map suitable for clj-http.."
  [method url access-token extra-params]
  (merge extra-params
         *default-http-params*
         {:method method
          :url url}
         (when (or (:form-params extra-params) (:body extra-params))
           {:content-type :json})
         (when access-token
           {:oauth-token access-token})))

(defn- build-request
  "Used by request and async-request before getting the body."
  [method context & args]
  {:pre [(let [{{:keys [base-url]} :server} context]
           (and (string? base-url) (seq base-url)))]}
  (let [[path extra-params] (if (map? (last args))
                              [(butlast args) (last args)]
                              [args {}])
        {{:keys [base-url]} :server access-token :access-token} context
        url (url-for base-url path)
        req (request-params method url access-token extra-params)]
    req))

(defn request
  "Wraps around clj-http.client/request to make interacting with APIs more
  convenient. Takes an HTTP method keyword (:get, :post…), a context object, the
  path to the endpoint as a list of path components (without /_matrix/client/r0)
  and additional parameters to be merged with the default HTTP parameters for
  clj-http.client. Returns the response body, if any.

  The base URL and, if applicable, the access token are taken from the context
  object.

  Examples:

  (request :post \"path\" \"to\" \"api\" \"endpoint\"
           {:query-params {:foo \"bar\"}})
  (request :post \"path\" \"to\" \"api\" \"endpoint\")
  "
  [method context & args]
  (-> (apply build-request method context args)
      client/request
      :body))

(defn async-request
  "As request, but enables the async feature. Returns an Apache BasicFuture."
  [method context & args]
  (binding [*default-http-params* (assoc *default-http-params* :async? true)]
    (let [req (apply build-request method context args)]
      (client/request (dissoc req :respond :raise)
                      (:respond req)
                      (:raise req)))))

(defhttpmethods :get :post :put :delete)
