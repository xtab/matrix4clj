(ns matrix4clj.util
  "Contains miscellaneous low-level utility functions used by other namespaces,
  notably matrix4clj.client. Some of them involve the client API."
  (:require [matrix4clj.api :as api]
            [clojure.string :as str]))

(defn config->login
  "Creates a JSON object suitable for use with the login API endpoint."
  [config]
  (let [{{{user :name} :user password :password} :login
         {device-id :id initial-device-display-name :display-name} :device} config]
    {:type :m.login.password
     :identifier
     {:type :m.id.user
      :user user}
     :password password
     :device_id device-id
     :initial_device_display_name initial-device-display-name}))

(defn invalidate-session
  "Removes session information (i.e. access token) from a context, e.g. because
  we have logged out."
  [context]
  (dissoc context :access-token))

(defn logged-in?
  "Tells whether the context represents a logged-in state."
  [context]
  (boolean (:access-token context)))

(defn create-context
  "Builds an initial context object out of a configuration map."
  [config]
  ;; TODO: We should take a full user ID (e.g. @foo:example.com) in the
  ;; configuration and support resolving the base URI with
  ;; /.well-known/matrix/client. That would make entering the server base URI
  ;; optional.
  (let [{:keys [homeserver]} config]
    {:config config
     :server {:base-url homeserver}}))

(defn keyword-to-string-map
  "Converts a map using keywords as keys, to a map using strings as keys (without
  the preceding colon). Namespaced keywords are converted to <namespace>/<name>
  form.

  This function does not change the mapped values. For example, {:a \"b\" :c
  {:d \"e\"}} is transformed into {\"a\" \"b\" \"c\" {:d \"e\"}}."
  [coll]
  (letfn [(keyword-to-string [x] (let [n (name x)
                                       ns (namespace x)]
                                   (str (when ns (str ns "/"))
                                        n)))]
    (into {} (map (fn [[k v]] [(keyword-to-string k) v])) coll)))

(defn whoami
  "Returns the Matrix user ID we are currently logged in as."
  [context]
  (:user-id context))

(defn whoami*
  "Asks the server for our user-ID."
  [context]
  (-> context
      (api/get "account" "whoami")
      :user_id))

(defn ensure-context-contains-user-id
  "After login, the context object should contain a user ID. If not, ask it from
  the server and add it to the object."
  [context]
  (if (not (:user-id context))
    (assoc context :user-id (whoami* context))
    context))

(defn room-id?
  "Checks if the given string looks like a room ID."
  [s]
  (boolean (re-matches #"![^:]+:[^:]+" s)))

(defn room-alias?
  "Checks if the given string looks like a room alias."
  [s]
  (boolean (re-matches #"#[^:]+:[^:]+" s)))

(defn resolve-room-alias
  "If given a room alias, resolves it to a room ID. If given a room ID, returns it as is."
  [context s]
  {:pre [(or (room-id? s) (room-alias? s))]
   :post [(room-id? %)]}
  (if (room-alias? s)
    (:room_id (api/get context "directory" "room" s))
    s))

(defn gen-txn-id
  "Generates a random transaction ID for sending events to a room."
  []
  (str "m-" (java.util.UUID/randomUUID)))
