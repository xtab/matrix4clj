(ns matrix4clj.core
  (:require [matrix4clj.account :refer [login logout logout-all]]
            [matrix4clj.util :refer [create-context resolve-room-alias gen-txn-id whoami]]
            [matrix4clj.event-chan :as event]
            [matrix4clj.api :as api]
            [matrix4clj.util :as util]))


(defn start
  "Starts a bot instance. Returns a context object representing that bot
  instance.

  The handlerfn must be a function of two arguments ([context event])."
  [config handlerfn]
  (let [before-login (create-context config)
        after-login (-> before-login login util/ensure-context-contains-user-id)
        after-start (merge after-login
                           (event/start-event-loop after-login handlerfn))]
    after-start))

(defn stop
  "Stops a bot instance."
  [context]
  (let [{:keys [channel sync-loop-thread]} context]
    (event/stop-event-loop {:channel channel :sync-loop-thread sync-loop-thread})
    (dissoc context :channel :sync-loop-thread)))

(defn join
  "Joins a room, designated either by its ID or an alias. Returns its room ID."
  [context room-id-or-alias]
  (-> context
      (api/post "join" room-id-or-alias)
      :room_id))

(defn send-event
  "Sends an event to a room. Returns the event ID."
  [context room-id-or-alias event-type event-content]
  (let [room-id (resolve-room-alias context room-id-or-alias)]
    (-> context
        (api/put "rooms" room-id "send" event-type (gen-txn-id)
                 {:form-params event-content})
        :event_id)))

(defn make-message
  "Makes a message object suitable for sending through the Matrix API. If given an
  existing object, does nothing."
  [message]
  (if (string? message)
    {:msgtype "m.text" :body message}
    message))

(defn message
  "Posts a message on a room. Returns the event ID."
  [context room-id-or-alias message]
  (send-event context room-id-or-alias "m.room.message" (make-message message)))

(defn reply
  "Posts a message on the same room as a previous message ID."
  [context event msg]
  (let [room (:room_id event)]
    (message context room msg)))

(defn send-receipt
  "Send a receipt for an event in a room."
  [context room-id receipt-type event-id]
  (api/post context "rooms" room-id "receipt" (name receipt-type) event-id))

(defn mark-as-read
  "Marks an event as read (usually message events)."
  ([context event]
   (mark-as-read context (:room_id event) (:event_id event)))
  ([context room-id event-id]
   (send-receipt context room-id :m.read event-id)))
